//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//      http://vispa.physik.rwth-aachen.de/ -
// Copyright (C) 2009-2012 Martin Erdmann   -
//               RWTH Aachen, Germany       -
// Licensed under a LGPL-2 or later license -
//-------------------------------------------

#include "pxl/core.hh"
#include "pxl/Root.hh"
#include "pxl/RootSerializable.hh"

namespace pxl
{

static bool _initialized = false;

static ObjectProducerTemplate<RootSerializable> _RootSerializableProducer;

void Root::initialize()
{
	if (_initialized)
		return;

	_RootSerializableProducer.initialize();
	_initialized = true;
}

void Root::shutdown()
{
	if (_initialized == false)
		return;
	_RootSerializableProducer.shutdown();
	_initialized = false;
}

} // namespace pxl
