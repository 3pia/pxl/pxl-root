//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//      http://vispa.physik.rwth-aachen.de/ -
// Copyright (C) 2009-2012 Martin Erdmann   -
//               RWTH Aachen, Germany       -
// Licensed under a LGPL-2 or later license -
//-------------------------------------------

#ifndef PXL_ROOT_HH
#define PXL_ROOT_HH

#include "pxl/RootSerializable.hh"
#include "pxl/Root.hh"

#endif // PXL_ROOT_HH
