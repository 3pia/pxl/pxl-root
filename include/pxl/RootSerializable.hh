//-------------------------------------------
// Project: Physics eXtension Library (PXL) -
//      http://vispa.physik.rwth-aachen.de/ -
// Copyright (C) 2009-2012 Martin Erdmann   -
//               RWTH Aachen, Germany       -
// Licensed under a LGPL-2 or later license -
//-------------------------------------------

#ifndef PXL_ROOT_ROOTSERIALIZABLE_HH
#define PXL_ROOT_ROOTSERIALIZABLE_HH

#include <iostream>
#include <string>
#include <stdexcept>

#include "pxl/core/Serializable.hh"
#include "pxl/core/Id.hh"

class TObject;

namespace pxl
{


/** 
 This class can serialise arbitrary ROOT objects deriving from TObject, e.g. histogram
classes (TH1) and graphs. 
The RootSerializable assumes ownership of the TObject, i.e. it takes deletion 
responsibilities. The TObject may hence not be owned by a ROOT class, e.g. a TFile.
Note that complex ROOT classes like e.g. TTrees may not be properly written to disk.
In addition, all links to other ROOT objects will be lost in the serialization.
 */
class PXL_DLL_EXPORT RootSerializable : public Serializable
{
public:
	/// Default constructor.
	RootSerializable(const std::string& name = "") :
	Serializable(),
	_name(name),
	_tobject(0)
	{
	}
	
	/// Constructor from TObject. Note that this constructor
	/// is not available in Python - use the default constructor
	/// and setTObject().
	RootSerializable(TObject* tobj, const std::string& name = "");
	
	/// Copy constructor.
	RootSerializable(const RootSerializable& original);

	/// Copy constructor. 
	explicit RootSerializable(const RootSerializable* original);
	
	/// Destructor, ensures safe deletion of all hard relations.
	virtual ~RootSerializable();

	/// Returns the PXL unique object-id (UUID)
	inline Id id() const
	{
		return getId();
	}

	static const Id& getStaticTypeId()
	{
		static const Id id("653debf0-e0a8-11e1-9b23-0800200c9a66");
		return id;
	}

	/// Returns the unique ID of this class
	virtual const Id& getTypeId() const
	{
		return getStaticTypeId();
	}

	virtual void serialize(const OutputStream &out) const;

	virtual void deserialize(const InputStream &in);

	/// Creates a deep copy and returns a C++ pointer to the newly-created object.  
	virtual Serializable* clone() const
	{
		return new RootSerializable(*this);
	}

	/// Returns the name.
	inline const std::string& getName() const
	{
		return _name;
	}

	/// Sets the name to the contents of \p name.
	inline void setName(const std::string& name)
	{
		_name = name;
	}
	
	/// Get contained TObject. In case there is no TObject, a null pointer is returned.
	TObject* getTObject()
	{
		return _tobject;
	}
	
	/// Set contained TObject. If TObject present, it is deleted.
	/// Note that the TObject is assumed to have no owner. This can
	/// be assured by setting the directory to 0 in ROOT if the TObject
	/// derivative has such at mehod. Otherwise, the TObject may have to be created 
	/// without an open TFile 
	void setTObject(TObject* tobj);

	/// Take contained TObject. The TObject is taken from the RootSerializable,
	/// and set to a null pointer. The caller takes deletion responsibility.
	/// In case there is no TObject, a null pointer is returned.
	TObject* takeTObject();

	/// Prints out object state information on various verbosity levels.
	/// @param level verbosity level
	/// @param os output _stream, default is std::cout
	/// @param pan print indention
	/// @return output _stream
	virtual std::ostream& print(int level = 1, std::ostream& os = std::cout,
			int pan = 0) const;



private:
	std::string _name; // Name of the RootSerializable instance
	TObject* _tobject; // Contained TObject. Note that ownership is assumed.

	/// No assignment of RootSerializable derivatives is allowed, the assignment
	/// operator is private
	RootSerializable& operator=(const RootSerializable& original)
	{
		return *this;
	}
};

}
// namespace pxl

// operators
PXL_DLL_EXPORT std::ostream& operator<<(std::ostream& cxxx, const pxl::RootSerializable& obj);

#endif // PXL_ROOT_ROOTSERIALIZABLE_HH
